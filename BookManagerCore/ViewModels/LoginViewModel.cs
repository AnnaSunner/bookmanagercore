﻿namespace BookManagerCore.ViewModels
{
    public class LoginViewModel
    {
        public string Login { get; set; }
        public string Password { get; set; }
        public bool Remember { get; set; }
    }
}
