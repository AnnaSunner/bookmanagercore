﻿using AutoMapper;
using BookManagerCore.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BookManagerCore.ViewModels
{
    public class DomainProfile: Profile
    {

		public DomainProfile()
		{		
		this.CreateMap<RegisterViewModel, User>();
			this.CreateMap<CreateBookViewModel, Book>();
			this.CreateMap<Book, BookViewModel>();
			this.CreateMap<LoginViewModel, User>().ReverseMap();
		}

	}
}
