﻿using System;

namespace BookManagerCore.ViewModels
{
    public class BookViewModel
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string Author { get; set; }
        public int Rating { get; set; }
        public Guid CategoryId { get; set; }

		public Guid UserId { get; set; }
	}
}
