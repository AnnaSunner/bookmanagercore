﻿using System;

namespace BookManagerCore.ViewModels
{
    public class CreateBookViewModel
    {
        public string Name { get; set; }
        public string Author { get; set; }
        public int Rating { get; set; }
        public Guid CategoryId { get; set; }
    }
}
