﻿using BookManagerCore.Data;
using BookManagerCore.Domain;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace BookManagerCore
{   
    public class InitData
    { 
        public static void Seed(ApplicationDBContext context)
		{
			if(!context.Users.Any())
			{
				context.Add(
					new User
					{
                        Id = Guid.NewGuid(),
                        Name = "Lena",
                        PasswordHash = "Qwe123"
					});
				context.SaveChanges();
			}

			if (!context.Categories.Any())
			{
				context.AddRange(
					new Category
					{
                        Id = Guid.NewGuid(),
                        Name = "Reading"
					},
					new Category
					{
                        Id = Guid.NewGuid(),
                        Name = "Future reading"
					},
					new Category
					{
                        Id = Guid.NewGuid(),
                        Name = "Already reading"
					});
				context.SaveChanges();
			}

			if(! context.Books.Any())
			{
				context.AddRange(
					new Book
					{
                        Id = Guid.NewGuid(),
                        Name = "soup for the soul",
						Author = "qaaa",
						Rating = 5						
					},

					new Book
					{
                        Id = Guid.NewGuid(),
                        Name = "Book2",
						Author = "author2",
						Rating = 1
					});
				context.SaveChanges();
			}

			context.Database.ExecuteSqlCommand(@"
                CREATE TABLE log (
                  Id int(10) unsigned NOT NULL AUTO_INCREMENT,
                  UtcDate datetime DEFAULT NULL,
                  Level varchar(50) DEFAULT NULL,
                  Message varchar(512) DEFAULT NULL,
                  Logger varchar(250) DEFAULT NULL,
                  Callsite varchar(512) DEFAULT NULL,
                  Exception varchar(512) DEFAULT NULL,
                  PRIMARY KEY (Id)
                );");
			context.SaveChanges();
		}
    }
}
