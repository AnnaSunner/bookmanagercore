﻿using Microsoft.AspNetCore.Diagnostics;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BookManagerCore.Controllers
{
    public class ErrorController: Controller
    {
        private readonly ILogger<AccountController> logger;
        public ErrorController(ILogger<AccountController> logger)
        {
            this.logger = logger;
        }

        [HttpGet("error")]
        public IActionResult Error()
        {
            var feature = HttpContext.Features.Get<IExceptionHandlerFeature>();
            var exception = feature?.Error;

            if (exception != null)
            {
                logger.LogError(exception, exception.Message);
            }

            return this.View();
        }
    }
}
