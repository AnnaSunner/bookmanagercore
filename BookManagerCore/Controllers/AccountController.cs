﻿using BookManagerCore.Domain;
using BookManagerCore.Services.Authentication;
using BookManagerCore.Services.Hashing;
using BookManagerCore.Services.Users;
using BookManagerCore.ViewModels;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Security.Claims;
using System.Threading.Tasks;

namespace BookManagerCore.Controllers
{
    public class AccountController: Controller
    {
        private readonly ILogger<AccountController> logger;
        private readonly UserManager userManager;
        private readonly SignInManager signInManager;

        public AccountController(ILogger<AccountController> logger, UserManager userManager, SignInManager signInManager)
        {
            this.logger = logger;
            this.userManager = userManager;
            this.signInManager = signInManager;
        }

        [HttpGet("main")]
        public IActionResult Main()
        {
            return View();
        }

        [HttpGet("register")]
        public IActionResult Register()
        {
            return View();
        }

        [HttpGet("index")]
        public IActionResult Index()
        {
            return View("Index");
        }

        [HttpPost("register")]
        [ValidateAntiForgeryToken]
        public IActionResult Register(RegisterViewModel registerViewModel)
        {
            if (ModelState.IsValid)
            {
                User user =  userManager.GetByLogin(registerViewModel.Login);
                if (user == null)
                {
                    if (registerViewModel.Password == registerViewModel.ConfirmPassword)
                    {
						user.Id = Guid.NewGuid();
                        var identityResult = userManager.Create(user, registerViewModel.Password);
                        if (identityResult.Succeeded)
                        {
                            return RedirectToAction("/");
                        }
                    }
                }
                else
                    ModelState.AddModelError("", "Некорректные логин и(или) пароль");
            }
                return View("main");
        }

        [HttpGet("login")]
        public IActionResult LogIn([FromQuery] string returnUrl = null)
        {

            ViewBag.ReturnUrl = returnUrl;

            return View();
        }

        [HttpPost("login")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> LogInAsync(LoginViewModel loginViewModel, [FromForm] string returnUrl = null, bool Remember = false)
        {
            User user = userManager.GetByLogin(loginViewModel.Login);
            if(user == null)
            {
                return View();
            }
            string userData;
            PasswordHasher passwordHasher = new PasswordHasher();
            if (loginViewModel.Login == user.Name && passwordHasher.HashPassword(loginViewModel.Password) == user.PasswordHash)
            {
                if (returnUrl != null)
                {
                    userData = JsonConvert.SerializeObject(
                        new CookieData
                        {
                            UserId = user.Id,
                            Login = user.Name
                        });
                    return this.Redirect(returnUrl);
                }
                await Authenticate(loginViewModel.Login);
            }
            return RedirectToAction("Index","Books", new { idUser = user.Id});
        }

        [Authorize]
        [HttpGet("logout")]
        public async Task<IActionResult> LogOutAsync()
        {
            await HttpContext.SignOutAsync(CookieAuthenticationDefaults.AuthenticationScheme);
            return RedirectToAction("Main");
        }


        private async Task Authenticate(string login)
        {
            var claims = new List<Claim>
            {
                new Claim(ClaimsIdentity.DefaultNameClaimType, login)
            };
            ClaimsIdentity id = new ClaimsIdentity(claims, "ApplicationCookie", ClaimsIdentity.DefaultNameClaimType, ClaimsIdentity.DefaultRoleClaimType);
            await HttpContext.SignInAsync(CookieAuthenticationDefaults.AuthenticationScheme, new ClaimsPrincipal(id));
        }
    }
}

