﻿using BookManagerCore.Services.Books;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using BookManagerCore.ViewModels;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BookManagerCore.Domain;
using AutoMapper;

namespace BookManagerCore.Controllers
{
    public class BooksController: Controller
    {
        private readonly ILogger<BooksController> logger;
        private readonly BookManager bookManager;
        private readonly Guid idUser;
		private readonly Mapper mapper;

        public BooksController(ILogger<BooksController> logger, BookManager bookManager, Guid idUser)
        {
            this.logger = logger;
            this.bookManager = bookManager;
            this.idUser = idUser;
        }

        [HttpGet("books/list")]
        public IActionResult Index(Guid idUser)
        {
            return View(bookManager.GetBooks(idUser));
        }

        [HttpGet("books/create")]
        public IActionResult Create()
        {
            return View();
        }



        [HttpPost("books/create")]
        [ValidateAntiForgeryToken]
        public IActionResult Create(Book book)
        {
			try
			{
				book.UserId = idUser;
				book.Id = Guid.NewGuid();
				bookManager.Create(book);
				ViewBag.ButtonName = "Create";
				return View("Create");
			}
			catch(Exception ex)
			{
				this.logger.LogError(
					string.Empty,
					"Unable to add book");
				return this.View(book);
			}
        }


        [HttpGet("books/{id:guid}")]
        public IActionResult BookInfo(Guid id)
        {
            var book = bookManager.FindById(id);
            if (book.UserId == idUser)
            {
                return View();
            }
			
				return RedirectToAction(nameof(Index));	            
        }

        [HttpGet("books")]
        public IActionResult GetBooks(int pageSize, int page)
        {
			var filter = new BookFilter
			{
				UserId = this.idUser,
				Skip = (page -1)*pageSize,
				Take = 5
			};

			var books = this.bookManager.GetBooks(filter);
				
            return Json(books);

        }

        [HttpGet("books/edit/{id:guid}")]
        public IActionResult Edit(Guid id)
        {
            ViewBag.ButtonName = "Update";
            return View("Create");
        }

        [HttpPost("books/edit")]
        [ValidateAntiForgeryToken]
        public IActionResult Update(Book book)
        {
			if (book != null)
			{
				if (book.UserId == idUser)
				{
					bookManager.Update(book);
					ViewBag.ButtonName = "Update";
					return View("Create");
				}
			}

			this.ModelState.AddModelError(
				string.Empty,
				"you haven't book");
            return this.Redirect("/error");
        }

        [HttpPost("books/delete")]
        public IActionResult Delete(Guid id)
        {
            var book = bookManager.FindById(id);
            if (book.UserId == idUser)
            {
                bookManager.Delete(book);
                return RedirectToAction("Index");
            }
            return this.Redirect("/error");
        }
    }
}

