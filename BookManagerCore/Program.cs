﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper.Configuration;
using BookManagerCore.AppStart;
using BookManagerCore.Data;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using NLog.Web;

namespace BookManagerCore
{
	public class Program
	{
		public static void Main(string[] args)
		{
			var host = BuildWebHost(args);

            InitDb(host);

            host.Run();
		}

		public static IWebHost BuildWebHost(string[] args) =>
			new WebHostBuilder()
			.UseEnvironment(EnvironmentName.Development)
			.UseKestrel()
			.UseContentRoot(Directory.GetCurrentDirectory())
			.UseStartup<Startup>()
			.ConfigureAppConfiguration(AppConfiguration.Configure)
			.ConfigureLogging((webhostContext, builder) =>
			{
				var configuration = webhostContext.Configuration;

				builder
					.SetMinimumLevel(LogLevel.Trace)
					.AddConfiguration(configuration.GetSection("Logging"))
					.AddConsole()
					.AddDebug();
			})
			.UseNLog()
            .Build();

        public static void InitDb(IWebHost host)
        {
            using (var scope = host.Services.CreateScope())
            {
                var services = scope.ServiceProvider;
                try
                {
                    var context = services.GetRequiredService<ApplicationDBContext>();
                    context.Database.Migrate();
                    InitData.Seed(context);                   
                }
                catch (Exception ex)
                {
                    var logger = services.GetRequiredService<ILogger<Program>>();
                    logger.LogError(ex, "An error occurred while seeding the database.");
                }
            }
        }
	}
}
