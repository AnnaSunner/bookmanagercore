﻿using BookManagerCore.Domain;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BookManagerCore.ViewModels;

namespace BookManagerCore.Data
{
    public class ApplicationDBContext: DbContext
    {
		public ApplicationDBContext(DbContextOptions<ApplicationDBContext> options): base(options)
		{

		}

		public DbSet<User> Users { get; set; }
		public DbSet<Category> Categories { get; set; }
		public DbSet<Book> Books { get; set; }


		protected override void OnModelCreating(ModelBuilder modelBuilder)
		{
			base.OnModelCreating(modelBuilder);

			modelBuilder.Entity<User>()
				.Property(x => x.Name)
				.IsRequired();

            modelBuilder.Entity<User>()
                .Property(x => x.PasswordHash)
                .IsRequired();

            modelBuilder.Entity<Category>()
				.Property(x => x.Name)
				.IsRequired();

			modelBuilder.Entity<Book>()
				.Property(x => x.Name)
				.IsRequired();

			modelBuilder.Entity<Book>()
				.Property(x => x.Author)
				.IsRequired();

			modelBuilder.Entity<Book>()
				.HasOne(x => x.User)
				.WithMany()
				.HasForeignKey(x => x.UserId);

			modelBuilder.Entity<Book>()
				.HasOne(x => x.Category)
				.WithMany()
				.HasForeignKey(x => x.CategoryId);
		}		
    }
}
