﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace BookManagerCore.AppStart
{
    public static class AppConfiguration
    {
		public static void Configure(WebHostBuilderContext context, IConfigurationBuilder builder)
		{
			var env = context.HostingEnvironment;

			builder
				.SetBasePath(Directory.GetCurrentDirectory()) // берем настройки конфигурации из внешнего ресурса
				.AddJsonFile("appsettings.json")
				.AddJsonFile($"appsettings.{env.EnvironmentName}.json", true, false);
		}
    }
}
