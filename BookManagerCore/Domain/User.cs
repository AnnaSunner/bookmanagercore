﻿using System;
using System.Collections.Generic;

namespace BookManagerCore.Domain
{
	public class User
    {
		public Guid Id { get; set; }		
		public string Name { get; set; }		
		public string PasswordHash { get; set; }
	}
}
