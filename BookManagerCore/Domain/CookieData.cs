﻿using System;

namespace BookManagerCore.Domain
{
    internal class CookieData
    {
        public Guid UserId { get; set; }
        public string Login { get; set; }
    }
}
