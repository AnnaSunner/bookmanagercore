﻿using System;

namespace BookManagerCore.Domain
{
	public class Book
    {
		public Guid Id { get; set; }		
		public string Name { get; set; }		
		public string Author { get; set; }
		public int Rating { get; set; }
		public Guid CategoryId { get; set; }
		public Guid UserId { get; set; }

		public virtual Category Category { get; set; }
		public virtual User User { get; set; }
	}
}
