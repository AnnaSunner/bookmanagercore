﻿using BookManagerCore.Data;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Configuration;
using System.IO;
using Microsoft.AspNetCore.Authentication.Cookies;
using BookManagerCore.Services.Hashing;
using BookManagerCore.Services.Users;
using BookManagerCore.Services.Authentication;
using BookManagerCore.Services.Books;
using Microsoft.Extensions.Logging;
using BookManagerCore.ViewModels;
using FluentValidation.AspNetCore;
using BookManagerCore.Controllers;
using BookManagerCore.Validators;
using FluentValidation;
using Microsoft.AspNetCore.Mvc;
using AutoMapper;

namespace BookManagerCore
{
    public class Startup
    {
        public IConfiguration Configuration { get; }
        private string secretCS = null;
        public Startup(IConfiguration configuration)
        {
			//var builder = new ConfigurationBuilder();

			//builder
			//    .SetBasePath(Directory.GetCurrentDirectory()) // берем настройки конфигурации из внешнего ресурса
			//    .AddJsonFile("appsettings.json")
			//    .AddJsonFile($"appsettings.{env.EnvironmentName}.json", true, false);


			//if (env.IsDevelopment())
			//{
			//    builder.AddUserSecrets<Startup>();
			//}

			//this.Configuration = builder.Build();

			this.Configuration = configuration;
        }

        public void ConfigureServices(IServiceCollection services)
        {            
            services.AddDbContext<ApplicationDBContext>(options =>
            {
                options.UseSqlServer(this.Configuration.GetConnectionString("DefaultConnection"));
            });
            services
				.AddAutoMapper()
               .AddAuthentication(CookieAuthenticationDefaults.AuthenticationScheme)
               .AddCookie(o => {
                   o.LoginPath = "/login";
               });

            services.AddMvc()
                .AddFluentValidation();
            services.AddSingleton<IHttpContextAccessor, HttpContextAccessor>();

            services.AddSingleton<IPasswordHasher, PasswordHasher>();
            services.AddScoped<UserManager>();
            services.AddScoped<SignInManager>();
            services.AddScoped<BookManager>();

            services.AddTransient<IValidator<LoginViewModel>, LoginValidator>();
            services.AddTransient<IValidator<RegisterViewModel>, RegisterValidator>();           
        }

    public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/error");
            }

            
            app
				.UseStaticFiles()
               .UseAuthentication()
               .UseMvc()
               .Run(async (context) =>
               {
                   var logger = context.RequestServices.GetRequiredService<ILogger<Startup>>();
                   logger.LogInformation("Trace test");

                   await context.Response.WriteAsync("Hello World!");
               });
        }      
    }
        
    
}
