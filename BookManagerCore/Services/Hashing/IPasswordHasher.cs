﻿namespace BookManagerCore.Services.Hashing
{
    public interface IPasswordHasher
    {
        string HashPassword(string password);
    }
}
