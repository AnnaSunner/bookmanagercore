﻿using BookManagerCore.Domain;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace BookManagerCore.Services.Books
{
    public class BookFilter
    {
        public Guid UserId { get; set; }

        public int Skip { get; set; }

        public int Take { get; set; }

		public SortOrder SortOrder { get; set; } = SortOrder.Ascending;

        public Expression<Func<Book, bool>> Predicate { get; set; }
    }
}
