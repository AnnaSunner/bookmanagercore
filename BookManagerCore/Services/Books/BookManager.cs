﻿using BookManagerCore.Data;
using BookManagerCore.Domain;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Data.SqlClient;

namespace BookManagerCore.Services.Books
{
    public class BookManager
    {
        private readonly ApplicationDBContext context;

        public BookManager(ApplicationDBContext context)
        {
            this.context = context;
        }
        public void Create(Book book)
        {
			book.Id = Guid.NewGuid();
            context.Add(book);
			context.SaveChanges();
        }

        public void Update(Book book)
        {
            if (FindById(book.Id) != null)
            {
                context.Entry(book).State = EntityState.Modified;
                context.SaveChanges();
            }
        }

        public void Delete(Book book)
        {
            context.Entry(book).State = EntityState.Deleted;
            context.SaveChanges();
        }

        public Book FindById(Guid id)
        {
            return context.Set<Book>().Find(id);
        }

		public IEnumerable<Book> GetBooks(BookFilter filter)
		{
			//filter.EnsureNotNull();
			if(filter == null)
			{
				throw new NullReferenceException("filter is null");
			}

			IQueryable<Book> queryable = this.context.Set<Book>();

			queryable = queryable.Where(book => book.UserId == filter.UserId);

			if(filter.Predicate != null)
			{
				queryable = queryable.Where(filter.Predicate);
			}

			if(filter.SortOrder != SortOrder.Unspecified)
			{
				queryable = filter.SortOrder == SortOrder.Descending
					? queryable.OrderByDescending(b => b.Name)
					: queryable.OrderBy(b => b.Name);
			}

			queryable = queryable.Skip(filter.Skip);

			if(filter.Take > 0)
			{
				queryable = queryable.Take(filter.Take);
			}

			return queryable.ToList<Book>();

		}

        public IEnumerable<Book> GetBooks(Guid idUser)
        {
            return context.Set<Book>().Where(b => b.UserId == idUser);
        }
    }
}
