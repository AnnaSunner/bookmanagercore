﻿using System.Collections.Generic;

namespace BookManagerCore.Services.Users
{
    public class IdentityResult
    {
        private readonly List<string> errors = new List<string>();

        public bool Succeeded { get; set; }

        public IEnumerable<string> Errors => this.errors.AsReadOnly();

        internal void AddError(string message)
        {
            this.errors.Add(message);
        }
    }
}
