﻿using BookManagerCore.Services.Hashing;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BookManagerCore.Domain;
using BookManagerCore.Data;
using Microsoft.Extensions.Logging;

namespace BookManagerCore.Services.Users
{
    public class UserManager
    {
        private readonly IPasswordHasher passwordHasher;
        private readonly ApplicationDBContext context;
        public UserManager(ApplicationDBContext context, IPasswordHasher passwordHasher)
        {
            this.passwordHasher = passwordHasher;
            this.context = context;
        }
        public IdentityResult Create(User user, string password)
        {
            IdentityResult identityResult = new IdentityResult();
            identityResult.Succeeded = false;
            try
            {
                user.PasswordHash = passwordHasher.HashPassword(password);
				user.Id = Guid.NewGuid();
                context.Add(user);
                identityResult.Succeeded = true;
            }
            catch (Exception ex)
            {
                identityResult.AddError(ex.Message);
                //logger.LogInformation(identityResult.Errors.ToString());
            }

            return identityResult;
        }

        public User GetByLogin(string login)
        {
            try
            {
                return context.Set<User>().Find(login);
            }
            catch (Exception ex)
            {
                //logger.LogInformation(ex.Message);
                return null;
            }
        }
    }
}
