﻿using BookManagerCore.Domain;
using BookManagerCore.Services.Hashing;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace BookManagerCore.Services.Authentication
{
    public class SignInManager
    {
        private readonly IPasswordHasher passwordHasher;
        private readonly ILogger<SignInManager> logger;
        private readonly IHttpContextAccessor contextAccessor;

        private HttpContext HttpContext => contextAccessor.HttpContext;


        public SignInManager(IPasswordHasher passwordHasher, ILogger<SignInManager> logger, IHttpContextAccessor contextAccessor)
        {
            this.passwordHasher = passwordHasher;
            this.logger = logger;
            this.contextAccessor = contextAccessor;
        }

        public async Task<bool> SignInAsync(User user, string password, bool isPersistent)
        {
            var hash = this.passwordHasher.HashPassword(password);

            if (hash != user.PasswordHash)
            {
                return false;
            }

            var claims = new[]
            {
                new Claim(ClaimTypes.Name, user.Name),
                new Claim(ClaimTypes.NameIdentifier, user.Id.ToString())
            };

            var identity = new ClaimsIdentity(claims,
				CookieAuthenticationDefaults.AuthenticationScheme);
            var principal = new ClaimsPrincipal(identity);

            await this.HttpContext.SignInAsync(principal, new AuthenticationProperties()
            {
                IsPersistent = isPersistent
            });

            return true;
        }

        
        public async System.Threading.Tasks.Task SignOutAsync(HttpContext context)
        {
            await context.SignOutAsync(
    CookieAuthenticationDefaults.AuthenticationScheme);
        }

		public Guid GetUserId(ClaimsPrincipal principal)
		{
			var claim = principal.FindFirst(ClaimTypes.NameIdentifier);

			if(claim == null || Guid.TryParse(claim.Value, out var id))
			{
				return Guid.Empty;
			}

			return id;
		}
    }
}
