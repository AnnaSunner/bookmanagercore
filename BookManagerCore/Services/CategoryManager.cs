﻿using BookManagerCore.Domain;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BookManagerCore.Services
{
    public class CategoryManager
    {
		private readonly ILogger logger;
		private readonly DbContext context;

		public CategoryManager(ILogger logger, DbContext context)
		{
			this.logger = logger;
			this.context = context;
		}

		public IEnumerable<Category> Catogories =>
			this.context.Set < Category>();

	}
}
