﻿using BookManagerCore.ViewModels;
using FluentValidation;

namespace BookManagerCore.Validators
{
    public class LoginValidator : AbstractValidator<LoginViewModel>
    {
        public LoginValidator()
        {
            this.RuleFor(x => x.Login).NotEmpty()
                .WithMessage("Login is required");
            this.RuleFor(x => x.Login)
                .Length(3, 200)
                .WithMessage("Login count more than 2 and less than 200 characters");
            this.RuleFor(x => x.Password).NotEmpty()
                .WithMessage("Password is required");
            this.RuleFor(x => x.Password)
                .Length(5, 30)
                .WithMessage("Password count more than 4 and less than 30 characters");
            this.RuleFor(x => x.Password)
                .Matches("(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])[0-9a-zA-Z]{5,}")
                .WithMessage($"The password contains from " +
                $"at least one number, uppercase and lowercase letters");

        }
    }
}
