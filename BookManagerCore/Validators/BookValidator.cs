﻿using BookManagerCore.ViewModels;
using FluentValidation;

namespace BookManagerCore.Validators
{
    public class BookValidator: AbstractValidator<BookViewModel>
    {
        public BookValidator() 
        {
            this.RuleFor(x => x.Name)
                .Length(5, 255)
                .Matches("(?=.*[a-z])+");

            this.RuleFor(x => x.Rating)
                .InclusiveBetween(0, 5);
        }
    }
}
